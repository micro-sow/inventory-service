# Service Boilerpalte

Service Boilerplate is a Laravel 8 fresh Instalation that contains all it is dependencies to act a micro service 

## Requirement

* Docker
* [Docker-Sync](https://docker-sync.readthedocs.io/en/latest/getting-started/installation.html)



## Installation

Change the values inside `.env.example` in both `root` and `src` folder

### MacOs

Run the below commands on MacOs

```make
make sync-start

make
```

### Windows

* Option 1: [How to run Makefile on Windows](https://dikaseba.blogspot.com/2019/02/how-to-run-makefile-in-windows.html)
    * Run the same commands as MacOs
* Option 2: Open Makefile and manually execute the below
    * ```sync-start``` command content in CMD
    * ```all``` command content in CMD



## Automation

Makefile provde command that allow to automate a lot of common procedures
* Installation
* Migration
* Swagger documentaiton
* Clear Laravel cache 

Read Makefile for the complete list


## Usage

* Download the Service Boilerplate rename the folder and unlink it from Git
* Change the values inside `.env.example` in both `root` and `src` folder
* Create a new Git Repository and link 
* Install the new Service using the installation guide 

