<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* @OA\Tag(
*     name="ItemController",
*     description="Item API Endpoints"
* )
*/
class ItemController extends BaseController
{
    /**
     * @OA\Get(
     *      path="/api/items/show/{id}",
     *      operationId="showItem",
     *      tags={"ItemController"},
     *      summary="show Item data",
     *      description="Returns Item data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Item id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation")
     *       )
     * )
     *
     * @param  string $key
     * @return \Illuminate\Http\Response
     */
    public function show(string $key)
    {
        dd();
        return [
            'key' => 'value',
        ];
    }
}
